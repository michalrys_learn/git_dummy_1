# Purpose
This is test repository for proof of concept: how to do an automatic update for several repositories (more than 50) based on one repository treated as a template repo.

# How to check it
1. This repo is a template. It has a correct file structure and a correct pom.xml file.
2. Use https://gitlab.com/michalrys_learn/git_dummy_2 as a repo where structure and pom.xml shall be updated.
3. Run bash script file, for example in gitBash: sh fileName.sh. Script content is presented below.

# Script
```
appBase="git_dummy_1"
appName="git_dummy_2"

git clone https://gitlab.com/michalrys_learn/$appBase
git clone https://gitlab.com/michalrys_learn/$appName

# create files structure and copy pom.xml
cd $appName
git checkout -b develop
cd ..
mkdir temp
mv $appName/* temp/
mkdir $appName/src
mkdir $appName/src/scripts
mv temp/* $appName/src/scripts/
cp $appBase/pom.xml $appName

# replace application name in pom.xml
sed -i "s/<artifactId>$appBase<\/artifactId>/<artifactId>$appName<\/artifactId>/" $appName/pom.xml

cd $appName
git commit -a -m "File structure was updated, pom.xml created."
#git push
cd ..
```
